
@Requirement [id=rq1] [description=Return a list where each element is a word from fname.]
Rationale  [id=ra1] [description=Define a function file2lst and it has a fname argument,create a list L. After open the fname, iterate over the list. Through strip() method removes the character specified at the beginning and end of a string, split() is a list that splits a string into multiple strings。 Then iterate word of the 1st,list L append x element. Finally close the fname and return L.] TestCase   [id=tc1] [description=checked whether each element of word from fname ]
Priority  [Medium] 

@Requirement [id=rq2] [description= Return a dictionary given list lst.  Each key is an element in the lst. The value is always 1.]
Rationale  [id=ra2] [description=Create a dictionary,iterate over the list, then d[w]=1,finally retuen d.]
TestCase   [id=tc2] [description=Find whether each key is an element in the lst.]
Priority   [Medium] 

@Requirement [id=rq3] [description=Return a dictionary where each key is a word both in the file fname and in the dictionary english_dictionary, and the corresponding value is the frequency of that word.]
Rationale  [id=ra3] [description=return a dictionary where each key is a word both in the file fname and in the dictionary english_dictionary, and the corresponding value is the frequency of that word. Judge if x is in d,then thed[x]=1,else the d[x]=d[x]+1, then return d ]
TestCase   [id=tc3] [description=checked the key in dictionary whether eack key ia s word in the file fname and in the dictionary english_dictionary ]
Priority   [Low]
@Requirement [id=rq4] [description=Return a sorted list of tuples, each tuple containing a key and a value.Note that the tuples are order in descending order of the value.]
Rationale  [id=ra4] [description=Define a function sort_by_value and then return a sorted list of tuples, each tuple containing a key and a value.]
TestCase   [id=tc4] [description=checked  whether the tuples are order in descending order of the value. ]
Priority   [High] 

